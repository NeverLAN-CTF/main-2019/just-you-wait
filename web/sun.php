<?php

class Sun {
	public $radius;
	public $x;
	public $y;
	public $line_extra = 50;

	function __construct($x, $y, $r){
		$this->radius = $r;
		$this->x = $x;
		$this->y = $y;
		$this->number_len = $r * 1.5;
		$this->gen_lines();
	}

	private function gen_lines(){
		for($i=1; $i<13; $i++){
			$this->lines[$i] = $this->get_line_coord($i);
		}
	}

	private function get_line_coord($num){
		$num_theta = (360/12)*$num;
		$num_y = ($this->number_len * cos(deg2rad($num_theta)));
		$num_x = ($this->number_len * sin(deg2rad($num_theta)));
		return array('x1'=>$this->x+$num_x-$this->line_extra, 'y1'=>$this->y+$num_y+$this->line_extra, 'x2'=>$this->x-$num_x+$this->line_extra, 'y2'=>$this->y-$num_y-$this->line_extra);
	}

	public function draw_sun(){
		$id = uniqid();
		$sun = '<g>
		<style>
		.line-'.$id.'{
			fill: #FFFF00;
			stroke: #FFFF00;
			stroke-width:2;
		}
		.sun-'.$id.'{
			fill: #FFFF00;
			stroke: #FFFF00;
		}
		</style>
		<circle class="sun sun-'.$id.'" r="'.$this->radius.'" cx="'.$this->x.'" cy="'.$this->y.'"/>
		';
		for($i=1; $i<13; $i++){
			$sun .= '
  <line y2="'.$this->lines[$i]['y2'].'" x2="'.$this->lines[$i]['x2'].'" y1="'.$this->lines[$i]['y1'].'" x1="'.$this->lines[$i]['x1'].'" class="line-'.$id.'"/>';
		}
		
		$sun .='
		</g>
		';
		return $sun;
	}
}
