<?php
require_once('clock.php');
require_once('sun.php');
date_default_timezone_set("Australia/Melbourne");
$time_zone_offset = 18; // This will offset the timezone so it will work with MST Time

$mst_hour = 2; // MST time
$minute = 25;  // MST time

$flag = "flag{3v3n_A_Br0ck3n_cl0ck_is_r1ght_tw1c3_a_d4y}";

// check that offset doesn't go past 24 hours
$hour = ($mst_hour + $time_zone_offset)%24;
// time_step is a 1 - 48 counter, it increments every half hour
$time_step = ((int)date('H')*2) + ((int)date('i')%30>1?1:0);
// build clock
$clock1 = new Clock(1098, 546, 10);
$clock1->set_time($hour, $minute);
// build sun
$sun = new Sun(50, 50, 100);

// debug
// This will override the time_step so we don't have to wait, just reload a bunch
/*
session_start();
if(!$_SESSION['t']){
	$_SESSION['t'] = 0;
}
$_SESSION['t'] = ($_SESSION['t'] + 1 )%48;
$time_step = $_SESSION['t'];
*/

// color pallets
$backgrounds = array("000000","140A0A","291414","3D1F1E","522928","673332","7B3E3C","904846","A55350","906865","7B7E7B","679391","52A9A7","3DBEBD","29D4D3","14E9E9","00FFFF","00FFFF","00FFFF","00FFFF","00FFFF","00FFFF","00FFFF","00FFFF","00FFFF","00FFFF","00FFFF","00FFFF","00FFFF","00FFFF","00FFFF","00FFFF","00FFFF","00DFDF","00BFBF","009F9F","007F7F","005F5F","003F3F","001F1F","000000","000000","000000","000000","000000","000000","000000","000000");
$grass_colors = array("005000","085000","105000","185000","205000","285000","305000","385000","405000","3C6500","387B00","349100","30A700","2CBD00","28D300","24E900","20FF00","1CFF00","18FF00","14FF00","10FF00","0CFF00","08FF00","04FF00","00FF00","00FF00","00FF00","00FF00","00FF00","00FF00","00FF00","00FF00","00FF00","00E900","00D300","00BD00","00A700","009100","007B00","006500","005000","005000","005000","005000","005000","005000","005000","005000");
$bedroom_colors = array("880800","880800","880800","880800","880800","880800","880800","880800","880800","880800","880800","880800","FF7F00","FF7F00","FF7F00","FF7F00","FF7F00","FF7F00","FF7F00","FF7F00","FF7F00","FF7F00","FF7F00","FF7F00","FF7F00","FF7F00","FF7F00","FF7F00","FF7F00","FF7F00","FF7F00","FF7F00","FF7F00","FF7F00","FF7F00","FF7F00","FF7F00","FF7F00","FF7F00","FF7F00","FF7F00","880800","880800","880800","880800","880800","880800","880800");
$office_colors = array("883333","883333","883333","883333","883333","883333","883333","883333","883333","883333","883333","883333","883333","883333","883333","FFAAAA","FFAAAA","FFAAAA","FFAAAA","FFAAAA","FFAAAA","FFAAAA","FFAAAA","FFAAAA","FFAAAA","FFAAAA","FFAAAA","FFAAAA","FFAAAA","FFAAAA","FFAAAA","FFAAAA","FFAAAA","FFAAAA","FFAAAA","FFAAAA","FFAAAA","FFAAAA","FFAAAA","883333","883333","883333","883333","883333","883333","883333","883333","883333");
// Color changing for night time
$background = $backgrounds[$time_step];
$grass_color = $grass_colors[$time_step];
// House animations
$bedroom_color = $bedroom_colors[$time_step];
$office_color = $office_colors[$time_step];
$lamp_opacity = ($time_step>38 || $time_step<15)?0.78:0.3;
// Night sky fading
$stars_opacity = ($time_step>35 || $time_step<8)?0.9:0.1;
// Sun animation
$sun_rotation = (360/48)*$time_step - 180 % 360;
// Cloud animation
$cloud_opacity = ($time_step>14 && $time_step<38)?0.9:0;
$cloud_color = "EEEEEE";
$cloud_x = ($time_step - 12)*(1080/12);
// All ciphers lead to rickroll
$ciphers = array(array("61 48 52 30 63 48 4d 36 4c 79 39 35 62 33 56 30 64 53 35 69 5a 53 39 6b 55 58 63 30 64 7a 6c 58 5a 31 68 6a 55 51 3d 3d", '18'), array('01100001 01001000 01010010 00110000 01100011 01001000 01001101 00110110 01001100 01111001 00111001 00110101 01100010 00110011 01010110 00110000 01100100 01010011 00110101 01101001 01011010 01010011 00111001 01101011 01010101 01011000 01100011 00110000 01100100 01111010 01101100 01011000 01011010 00110001 01101000 01101010 01010101 01010001 00111101 00111101','18'));
$cipher = $ciphers[($time_step>38 || $time_step<15)?0:1];
?>
<!DOCTYPE html>
<html>
  <head>
	<title></title>
	<meta http-equiv="refresh" content="60"/>
  </head>
  <body>
<?php if($hour%12 == ((int)date('h')%12) && $minute == (int)date('i')){
	?>
	<h1>Well would you look at the time...</h1>
	<p><?php echo($flag);?></p>
	<?php
}else{
?>
<svg width="1920" height="1080" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg">
 <defs>
  <pattern height="1.5%" width="5%" viewBox="0,0,15,2" id="bricks">
   <polygon id="brick" points="0,0 0,9 14,9 14,0" fill="#AA1010"/>
  </pattern>
  <pattern height="3%" width=".3%" viewBox="0,0,6,7" id="grassT">
   <line id="lgrass" stroke="rgb(0,210,0)" y2="0" x2="3" y1="5" x1="0"/>
   <line id="rgrass" stroke="rgb(0,200,0)" y2="7" x2="6" y1="0" x1="3"/>
  </pattern>
  <pattern id="starsT" viewBox="0,0,70,80" width="9%" height="8%" patternTransform="rotate(20) scale(1 1.5)">
	    <polygon points="0,0 4,5 0,10 5,6 10,10 6,5 10,0 5,4" fill="#AAAA00">
			<animate id="animation1"
		             attributeName="opacity"
		             from="0.5" to="1" dur="3s"
		             begin="0s;animation2.end" />
		    <animate id="animation2"
		             attributeName="opacity"
		             from="1" to="0.5" dur="2s" 
		             begin="animation1.end" />
		</polygon>
	    <polygon transform="rotate(45, 0, 0)" points="10,10 14,15 10,20 15,16 20,20 16,15 20,10 15,14" fill="#FFFFFF" fill-opacity="1">
			<animate id="animation3"
		             attributeName="opacity"
		             from="0.5" to="1" dur="2s"
		             begin="0s;animation4.end" />
		    <animate id="animation4"
		             attributeName="opacity"
		             from="1" to="0.5" dur="1.3s" 
		             begin="animation3.end" />
		</polygon>
  </pattern>
	<style type="text/css">@import url('http://fonts.googleapis.com/css?family=Reenie+Beanie');
 </defs>
<filter xmlns="http://www.w3.org/2000/svg" id="T4">
<feFlood x="0%" y="0%" width="100%" height="100%" flood-color="#ca6" result="one"/>
	
	<feTurbulence baseFrequency=".007,.25" numOctaves="1" seed="403"/>
<feColorMatrix type="matrix" result="turb1" values="   1   0   0  0  0      0   -1   0  0  0    0   0  -1  0  0      1   -1   1  0  0 "/>
<feMerge>
	<feMergeNode in="one"/>
	<feMergeNode in="turb1"/>
</feMerge>
</filter>
 <g>
  <title>Sky</title>
  <rect id="sky" height="1080" width="1920" y="0" x="0" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="5" stroke="#000000" fill="#<?php echo($background);?>"/>
  <rect id="stars" height="1080" width="1920" y="0" x="0" fill="url(#starsT)" fill-opacity="<?php echo($stars_opacity);?>"/>
 </g>
 <!-- rotate(0 90 480) would be noon-->
 <g transform="translate(910 200) rotate(<?php echo $sun_rotation;?> 90 480 )">
	<g width="400" height="400">
		<title>Sun</title>
		<?php echo $sun->draw_sun();?>
	</g>
	<g width="400" height="400">
		<title>Moon</title>
		<circle cx="50" cy="1030" r="100" fill="#DDDDDD"/>
		<circle cx="50" cy="1080" r="90" fill="#<?php echo($background);?>"/>
	</g>
 </g>
 <g>
  <title>Clouds</title>
  <ellipse ry="26" rx="27" id="svg_48" cy="185" cx="<?php echo($cloud_x);?>" fill="#<?php echo($cloud_color);?>" fill-opacity="<?php echo($cloud_opacity);?>"/>
  <ellipse ry="27" rx="34" id="svg_52" cy="173" cx="<?php echo($cloud_x+40);?>" fill="#<?php echo($cloud_color);?>" fill-opacity="<?php echo($cloud_opacity);?>" />
  <ellipse ry="22" rx="56.5" id="svg_56" cy="186" cx="<?php echo($cloud_x+43.5);?>" fill="#<?php echo($cloud_color);?>" fill-opacity="<?php echo($cloud_opacity);?>"/>
 </g>
 <g>
  <title>Grass</title>
  <rect id="grass" height="270" width="1920" y="850" x="0" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke="#000000" fill="#<?php echo($grass_color);?>"/>
  <rect id="grass" height="270" width="1920" y="850" x="0" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke="#000000" fill="url(#grassT)"/>
 </g>
 <g>
  <title>House</title>
  <rect id="wall" height="450" width="550" y="400" x="1000" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke="#000000" fill="#ffffaa"/>
  <polygon stroke="#000000" id="roof" points="1550,400,1000,400,1275,250 " fill="#FF1100"/>
  <polygon transform="translate(0.400452, 0.133484)" stroke="#000000" id="chimney" points="1445,243 1445,343 1345,288 1345,243 " fill="url(#bricks)"/>
 </g>
 <g>
  <title>door</title>
  <rect id="door" height="150" width="80" y="700" x="1235" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="null" stroke="#000000" fill="#7f3f00"/>
  <ellipse ry="5" rx="5" id="doorknob" cy="775" cx="1305" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="null" stroke="#000000" fill="#000000"/>
 </g>
 <g>
  <title>Office</title>
  <rect id="officeWallPaper" height="120" width="120" y="480" x="1066" stroke-width="0" stroke="#000000" fill="#<?php echo $office_color;?>"/>
  <rect id="Desk" height="13" width="41" y="585" x="1068" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="0" stroke="#000000" fill="#7f7f00"/>
  <rect id="LampPole" height="17" width="1.5" y="568" x="1070" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="0" stroke="#000000" fill="#7f7f7f"/>
  <rect id="Lamp" height="6" width="15" y="564" x="1070" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="0" stroke="#000000" fill="#007fff"/>
  <polygon id="LampLight" points="1082 570, 1087 584, 1072.5 584, 1074 570" fill="#FFFF00" opacity="<?php echo $lamp_opacity;?>"/>
  
  <rect id="Chair" height="20" width="3" y="580" x="1115" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="0" stroke="#000000" fill="#a86326"/>
 </g>
 <g>
  <title>Bedroom</title>
  <rect id="bedroomWallPaper" height="120" width="120" y="480" x="1360" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="0" stroke="#000000" fill="#<?php echo $bedroom_color;?>"/>
  <rect id="bed" height="13" width="18" y="586" x="1384" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="0" stroke="#000000" fill="#ffffff"/>
  <rect id="bed" height="15" width="68" y="584.11758" x="1400" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="0" stroke="#000000" fill="#0000ff"/>
  <rect id="bed" height="17" width="5" y="580" x="1467" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="0" stroke="#000000" fill="#7f0000"/>
  <rect id="bed" height="25" width="7" y="575" x="1379" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="0" stroke="#000000" fill="#7f0000"/>
  <ellipse fill="#ffffff" stroke="#000000" stroke-width="0" stroke-dasharray="null" stroke-linejoin="null" stroke-linecap="null" cx="1390" cy="584" id="pillow" rx="3.5" ry="1.75"/>
  <rect fill="#ffff00" stroke="#000000" stroke-width="0" stroke-dasharray="null" stroke-linejoin="null" stroke-linecap="null" x="1408" y="483" width="25" height="6" id="light"/></g>
 <g>
 <g>
  <title>Clock2</title>
	<?php echo $clock1->draw_clock();?>
 </g>
 <g>
  <title>OfficeWindow</title>
  <rect id="frame1" height="120" width="120" y="480" x="1066" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="6" stroke="#7f3f00" fill="none"/>
  <line id="vbar1" y2="600" x2="1126" y1="480" x1="1126" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="6" stroke="#7f3f00" fill="none"/>
  <line id="hbar1" y2="540" x2="1186" y1="540" x1="1066" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="6" stroke="#7f3f00" fill="none"/>
 </g>
 <g>
  <title>BedroomWindow</title>
  <rect id="svg_1" height="120" width="120" y="480" x="1360" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="6" stroke="#7f3f00" fill="none"/>
  <line id="svg_2" y2="600" x2="1420" y1="480" x1="1420" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="6" stroke="#7f3f00" fill="none"/>
  <line id="svg_3" y2="540" x2="1480" y1="540" x1="1360" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="6" stroke="#7f3f00" fill="none"/>
 </g>
 <g>
  <title>Sign</title>
  <rect id="svg_43" height="115" width="12" y="725" x="585" stroke-width="2" filter="url(#T4)"/>
  <rect id="svg_44" height="53" width="100" y="730" x="542" stroke-width="2" filter="url(#T4)"/>
  <switch>
    <foreignObject x="535" y="720" width="120" height="53">
      <p xmlns="http://www.w3.org/1999/xhtml" style="font-size:<?php echo($cipher[1]);?>;font-family:'Reenie Beanie';color:black;">To whomever it may concern:<br/><?php echo($cipher[0]);?></p>
    </foreignObject>
	<!-- some browsers wont handle foreignObject-->
    <text xml:space="preserve" text-anchor="middle" font-family="Reenie Beanie" font-size="<?php echo($cipher[1]);?>" id="svg_46" y="772" x="591" fill="#000000">To whomever it may concern: <?php echo($cipher[0]);?></text>
  </switch>
 </g>
</svg>
<?php
}
?>
  </body>
</html>
