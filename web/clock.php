<?php

class Clock {
	public $radius;
	public $minute_len;
	public $hour_len;
	public $x;
	public $y; 
	public $hour;
	public $minute;
	private $numbers = array();
	public $font_size;

	# styling
	public $font_color = '#000000';
	public $hour_color = '#000000';
	public $hour_width = 0.55;
	public $minute_color = '#000000';
	public $minute_width = 0.5;
	public $border_width = 0.5;
	public $border_color = '#000000';
	public $background_color = '#FFFFFF';

	function __construct($x, $y, $r){
		$this->radius = $r;
		$this->x = $x;
		$this->y = $y;
		$this->hour = (int)date('h');
		$this->minute = (int)date('i');
		$this->minute_len = $r/1.5; 
		$this->hour_len = $r/2.5; 
		$this->number_len = $r * 0.85; 
		$this->font_size = $r * .2; 
		$this->gen_number_loc();
	}

	public function set_time($hour, $min){
		$this->hour = $hour;
		$this->minute = $min;
		$this->gen_number_loc();
	}

	private function gen_number_loc (){
		for ($i=1; $i<13; $i++){
			$this->numbers[$i] = $this->get_num_coord($i);
		}
	}

	private function get_num_coord($num){
		$num_theta = (360/12)*$num;
		$num_y = ($this->number_len * cos(deg2rad($num_theta)));
		$num_x = ($this->number_len * sin(deg2rad($num_theta)));
		return array('x'=>$this->x+$num_x, 'y'=>$this->y-$num_y);
	
	}

	private function get_min_coord($min){
		$minute_theta = 6*$min;
		$minute_y = ($this->minute_len * cos(deg2rad($minute_theta)));
		$minute_x = ($this->minute_len * sin(deg2rad($minute_theta)));
		return array('x'=>$this->x+$minute_x, 'y'=>$this->y-$minute_y);
	}

	private function get_hour_coord($hour, $min){
		$hour_theta = (360/12)*$hour + 30 * ($min / 60);
		$hour_y = ($this->hour_len * cos(deg2rad($hour_theta)));
		$hour_x = ($this->hour_len * sin(deg2rad($hour_theta)));
		return array('x'=>$this->x+$hour_x, 'y'=>$this->y-$hour_y);
	}

	public function draw_clock(){
		$id = uniqid();
		$clock = '<g>
		<style>
		.num-'.$id.'{
			fill: '.$this->font_color.';
			font-size: '.$this->font_size.'px;
			text-anchor: "middle";
			font-family: "robot";
		}
		.hour-'.$id.'{
			stroke: '.$this->hour_color.';
			stroke-width: '.$this->hour_width.';
		}
		.minute-'.$id.'{
			stroke: '.$this->minute_color.';
			stroke-width: '.$this->minute_width.';
		}
		.circle-'.$id.'{
			fill: '.$this->background_color.';
			stroke: '.$this->border_color.';
			stroke-width: '.$this->border_width.';
		}
		</style>
		<circle class="circle circle-'.$id.'" cx="'.$this->x.'" cy="'.$this->y.'" r="'.$this->radius.'"/>';
		for($i=1; $i<13; $i++){
			$clock .= '<text class="num num-'.$id.'" text-anchor="middle" style="dominant-baseline: central;" y="'.$this->numbers[$i]['y'].'" x="'.$this->numbers[$i]['x'].'"  >'.$i.'</text>';
		}
		$minute = $this->get_min_coord($this->minute);
		$hour = $this->get_hour_coord($this->hour, $this->minute);
      $clock .='
			<line class="minute minute-'.$id.'" x1="'.$this->x.'" y1="'.$this->y.'" x2="'.$minute['x'].'" y2="'.$minute['y'].'"/>
			<line class="hour hour-'.$id.'" x1="'.$this->x.'" y1="'.$this->y.'" x2="'.$hour['x'].'" y2="'.$hour['y'].'"/>
		</g>
		';
		return $clock;
	}
}
?>
